$(function () {

    $('.parsing__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        fade: true,
        dotsClass: 'parsing-dots'
    });
    setTimeout(function () {
        let dots = document.querySelector('.parsing-dots');
        if (!dots) return
        let buttons = dots.querySelectorAll('button');

        buttons.forEach(btn => {
            let text = btn.innerHTML;
            btn.innerHTML = '<span>' + text + '</span>';
        });
    }, 50)

    $('.reviews__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        prevArrow: '<button type="button" class="slick-prev reviews-prev"><img src="img/reviews-prev.png" alt="Prev"></button>',
        nextArrow: '<button type="button" class="slick-next reviews-next"><img src="img/reviews-next.png" alt="Next"></button>',
    });

    $('.sales__slider').slick({
        slidesToScroll: 1,
        slidesToShow: 1,
        dots: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        dotsClass: 'sales-dots',
        prevArrow: '<button type="button" class="slick-prev sales-prev"><img src="img/sales-arrow.png" alt="Prev"></button>',
        nextArrow: '<button type="button" class="slick-next sales-next"><img src="img/sales-arrow.png" alt="Next"></button>',
        responsive: [
            {
                breakpoint: 600,
                settings: {
                    prevArrow: '<button type="button" class="slick-prev sales-prev"><img src="img/mini-prev.png" alt="Prev"></button>',
                    nextArrow: '<button type="button" class="slick-next sales-next"><img src="img/mini-next.png" alt="Next"></button>',
                }
            }
        ]
    })

    document.addEventListener('click', navigation);

    let headerSize;
    document.addEventListener('scroll', fixedMenu);

    let projectsList = document.querySelector('.projects__list');
    let currentProject = document.querySelector('.projects__list-item.active');
    if (projectsList) projectsList.addEventListener('click', projects);

    $('.parsing__item-main').magnificPopup({
        delegate: 'a',
        type: 'image'
    });
    let parsingList = document.querySelector('.parsing__slider');
    if (parsingList) parsingList.addEventListener('click', parsing);

    $('.gallery__inner').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true,
            tCounter: ''
        }
    });
    $('.gallery__btn').on('click', moreProjects);

    document.addEventListener('click', function () {

        if (!event.target.closest('.btn-modal')) return;

        event.preventDefault();

        let modal = document.querySelector('#default-modal');
        let content = document.querySelector('.form-module').innerHTML;

        openModal(modal, content);
    });

    document.addEventListener('change', function () {
        checkInput(event.target);
    });
    document.addEventListener('submit', checkForm);

    let errs = {};
    document.querySelectorAll('form').forEach((form) => {
        errs[form] = [];
    });

    document.addEventListener('click', menu);

    document.addEventListener('click', projectsSelect);

    checkWidth();
    window.addEventListener('resize', checkWidth);

    //Functions

    function checkWidth() {
        if (window.innerWidth < 750 && !($('.projects__box').hasClass('slick-slider'))) {
            $('.projects__box').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                nextArrow: '<button class="projects-next" type="button"><img src="img/projects-next.png" alt="Next"></button>',
                prevArrow: '<button class="projects-prev" type="button"><img src="img/projects-next.png" alt="Prev"></button>'
            });
        } else if (window.innerWidth >= 750 && $('.projects__box').hasClass('slick-slider')) {
            $('.projects__box').slick('unslick');
        }
        if (window.innerWidth > 768 && $('.header__wrapper').hasClass('fixed')) {
            let menu = document.querySelector('.menu');
            let header = document.querySelector('.header');
            let mb = parseInt(getComputedStyle(header).marginBottom, 10) + menu.offsetHeight + parseInt(getComputedStyle(menu).marginBottom, 10) + 'px';
            header.closest('.header__wrapper').classList.remove('fixed');
            document.querySelector('.document-wrapper').style.paddingTop = '';
            header.style.marginBottom = mb;
            menu.classList.add('fixed');
        } else if (window.innerWidth <= 768 && $('.menu').hasClass('fixed')) {
            let header = document.querySelector('.header');
            header.style.marginBottom = '';
            document.querySelector('.document-wrapper').style.paddingTop = menu.offsetHeight + parseInt(getComputedStyle(header).marginBottom, 10) + 'px';
            document.querySelector('.menu').classList.remove('fixed');
            header.closest('.header__wrapper').classList.add('fixed');
        }
    }

    function projectsSelect() {
        if (!event.target.closest('.projects__select')) return;

        let select = event.target.closest('.projects__select');
        select.classList.add('active');

        $('.projects__list').slideDown(300);
        document.querySelector('.projects__list').style.display = 'flex';

        document.addEventListener('click', closeProjects);
    }

    function closeProjects() {
        if (event.target.closest('.projects__select') || (event.target.tagName === 'A' && event.target.closest('.projects__list-item') || !event.target.closest('.projects__list-wrapper'))) {
            document.querySelector('.projects__select').classList.remove('active');
            $('.projects__list').slideUp(300);
            document.removeEventListener('click', closeProjects);
        }
    }

    function menu() {

        if (!event.target.closest('.header__menu-btn')) return;
        let menu = document.querySelector('.menu > ul').innerHTML;
        let mobile = document.querySelector('.mobile-menu');

        mobile.querySelector('.mobile-menu__list').innerHTML = menu;
        mobile.style.display = 'block';
        setTimeout(function () {
            mobile.classList.remove('off');
            mobile.classList.add('on');
        }, 50)

        document.addEventListener('click', closeMenu);
    }

    function closeMenu(event) {

        if (!event.target.closest('.close')) return;
        if (!event.target.closest('.mobile-menu')) return;

        let mobile = document.querySelector('.mobile-menu');
        mobile.classList.remove('on')
        mobile.classList.add('off');
        setTimeout(function () {
            mobile.style.display = 'none';
        }, 500)


        document.removeEventListener('click', closeMenu);
    }

    function navigation() {
        let target = event.target;

        if (target.tagName != 'A') return;
        if (!(target.closest('.menu') || target.closest('.mobile-menu'))) return;

        event.preventDefault();

        if (document.querySelector('.mobile-menu').style.display == 'block') {
            let close = document.querySelector('.mobile-menu').querySelector('.close');
            let event = {
                target: close
            }
            closeMenu(event);
        }

        let obj;
        let top;

        if (target.dataset.target == 'top') {
            top = 0;
        }
        else if (target.dataset.target == 'sales') {
            obj = document.querySelector('.sales');
            top = $(obj).offset().top - 150;
        } else {
            obj = document.querySelector('.' + target.dataset.target + '__title');
            top = $(obj).offset().top - 150;
        }

        $('html, body').animate({
            scrollTop: top
        }, 800);
    }

    function fixedMenu() {
        let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        let menu = document.querySelector('.menu');
        if (!menu) return
        if (getComputedStyle(menu).display == 'none') {
            menu = document.querySelector('.header');
        }
        let main = document.querySelector('.main').offsetHeight;
        if ((scrollTop > main) && !(menu.classList.contains('fixed') || (menu.closest('.header__wrapper') ? menu.closest('.header__wrapper').classList.contains('fixed') : false))) {
            if (menu.tagName === 'NAV') {
                let header = document.querySelector('.header');
                header.style.marginBottom = parseInt(getComputedStyle(header).marginBottom, 10) + menu.offsetHeight + parseInt(getComputedStyle(menu).marginBottom, 10) + 'px';
                menu.classList.add('fixed');
            } else if (menu.tagName === 'HEADER') {
                document.querySelector('.document-wrapper').style.paddingTop = menu.offsetHeight + parseInt(getComputedStyle(menu).marginBottom, 10) + 'px';
                menu.closest('.header__wrapper').classList.add('fixed');
            }
        } else if (scrollTop <= main && (menu.classList.contains('fixed') || (menu.closest('.header__wrapper') ? menu.closest('.header__wrapper').classList.contains('fixed') : false))) {
            if (menu.tagName === 'NAV') {
                menu.classList.remove('fixed');
                let header = document.querySelector('.header');
                header.style.marginBottom = '';
            } else if (menu.tagName === 'HEADER') {
                menu.closest('.header__wrapper').classList.remove('fixed');
                document.querySelector('.document-wrapper').style.paddingTop = '0px';
            }
        }
    }

    function projects() {
        if (!event.target.closest('.projects__list-item')) return;

        event.preventDefault();

        if (event.target.closest('.projects__list-item') === currentProject) return;

        let target = event.target;
        let content = document.querySelector('.' + target.dataset.target).innerHTML;
        currentProject.classList.remove('active')
        currentProject = target.closest('.projects__list-item');
        currentProject.classList.add('active');
        if (document.querySelector('.projects__box').classList.contains('slick-slider')) {
            $('.projects__box').slick('unslick');
        }
        document.querySelector('.projects__box').innerHTML = content;
        if (document.documentElement.offsetWidth < 750) {
            $('.projects__box').slick({
                slidesToScroll: 1,
                slidesToShow: 1,
                nextArrow: '<button class="projects-next" type="button"><img src="img/projects-next.png" alt="Next"></button>',
                prevArrow: '<button class="projects-prev" type="button"><img src="img/projects-next.png" alt="Prev"></button>'
            })
        }
    }

    function parsing() {
        if (!event.target.closest('.parsing__item-gallery'));
        if (event.target.tagName != 'IMG') return;

        let current = event.target.closest('.parsing__item-gallery').querySelector('.gallery__item.active');

        if (current.querySelector('img') == event.target) return;

        event.preventDefault();

        let src = event.target.src;
        let alt = event.target.alt;
        let img = document.createElement('img');
        img.src = src;
        img.alt = alt;
        current.classList.remove('active');
        event.target.closest('.gallery__item').classList.add('active');
        let box = event.target.closest('.parsing__item-images').querySelector('.parsing__item-main > a');
        box.href = src;
        box.innerHTML = '';
        box.append(img);
    }

    function moreProjects() {

        event.preventDefault();

        let extra = document.querySelector('.gallery__more').innerHTML;
        let box = document.querySelector('.gallery__inner');
        let target = event.target;
        target.closest('.gallery__btn-wrapper').style.display = 'none';
        box.innerHTML += extra;
        // setTimeout(function() {
        //     target.closest('.gallery__btn-wrapper').style.display = 'none';
        //     box.innerHTML += extra;
        //     box.querySelectorAll('.gallery__more-item').forEach(item => {
        //         item.classList.add('on');
        //     })
        // }, 250) 
    }


    function openModal(modal, content) {

        if (content) modal.querySelector('.modal__content').innerHTML = content;

        $(modal).find('input[type="tel"]').each(function () {
            $(this).mask('+7 (000) 000 00 00', {
                onChange: function (cep, e) {
                    checkPhone(cep, e);
                }
            });
        });

        let form = modal.querySelector('form');

        modal.style.display = 'flex';
        modal.querySelector('.modal__inner').classList.remove('thanks');
        setTimeout(function () {
            modal.classList.remove('off');
            modal.classList.add('on');
        }, 50);

        modal.addEventListener('click', function () {
            closeModal(event, modal);
        });
        document.addEventListener('keyup', function () {
            closeModal(event, modal);
        });

    }

    function closeModal(event, modal) {

        let target = event.target;

        if (!(target.closest('.close') || target.classList.contains('modal') || event.key == 'Escape')) return;

        if (target.closest('.close')) {
            event.preventDefault();
        }

        modal.classList.remove('on');
        modal.classList.add('off');
        setTimeout(() => {
            modal.style.display = 'none';
        }, 300);
        modal.removeEventListener('click', closeModal);
        document.removeEventListener('keyup', closeModal);
    }

    function checkInput(target) {
        let type = target.type;
        let form = target.closest('form');
        let regexp;
        let index = errs[form].indexOf(target);

        switch (type) {
            case 'text':
                regexp = /^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я\s]+$/;
                if (!target.value) {
                    form.nameValid = false;
                    target.closest('.input-wrapper').classList.remove('error');
                    target.closest('.input-wrapper').classList.remove('success');
                    if (index != -1) {
                        errs[form].splice(index, 1);
                    }
                }
                else if (regexp.test(target.value)) {
                    form.nameValid = true;
                    target.closest('.input-wrapper').classList.remove('error');
                    target.closest('.input-wrapper').classList.add('success');
                    if (index != -1) {
                        errs[form].splice(index, 1);
                    }
                }
                else {
                    form.nameValid = false;
                    target.closest('.input-wrapper').classList.remove('success');
                    target.closest('.input-wrapper').classList.add('error');
                    if (!errs[form].includes(target)) {
                        errs[form].push(target);
                    }
                }
                break;
        }
    }

    function checkPhone(value, event) {
        let target = event.target;
        let form = target.closest('form');
        let index = errs[form].indexOf(target);

        if (target.value.length == 0) {
            form.phoneValid = false;
            target.closest('.input-wrapper').classList.remove('error');
            target.closest('.input-wrapper').classList.remove('success');
            if (index != -1) {
                errs[form].splice(index, 1);
            }
        }
        else if (target.value.length == 18) {
            form.phoneValid = true;
            target.closest('.input-wrapper').classList.remove('error');
            target.closest('.input-wrapper').classList.add('success');
            if (index != -1) {
                errs[form].splice(index, 1);
            }
        }
        else {
            form.phoneValid = false;
            target.closest('.input-wrapper').classList.remove('success');
            target.closest('.input-wrapper').classList.add('error');
            if (!errs[form].includes(target)) {
                errs[form].push(target);
            }
        }
    }

    function checkForm() {
        event.preventDefault();

        let form = event.target;

        let data = new FormData();
        if (form.name) {
            if (!(form.nameValid) && !(errs[form].includes(form.name))) {
                errs[form].push(form.name);
            } else {
                data.append('name', form.name.value);
            }
        }
        if (form.phone) {
            if (!(form.phoneValid) && !(errs[form].includes(form.phone))) {
                errs[form].push(form.phone);
            } else {
                data.append('phone', form.phone.value);
            }
        }
        if (errs[form].length != 0) {
            for (err of errs[form]) {
                if (err.name != 'confirm') {
                    err.closest('.input-wrapper').classList.remove('success');
                    err.closest('.input-wrapper').classList.add('error');
                }
            }
            return;
        }


        // data.append('action', 'send_form');
        // data.append('nonce', ajax_query.nonce);

        // $.ajax({
        //     url: ajax_query.url,
        //     data: data,
        //     method: 'POST',
        //     contentType: false,
        //     processData: false,
        //     dataType: 'text',
        //     success: function (resp) {
        //         let modalBody = form.closest('.modal').querySelector('.modal__content');
        //         modalBody.classList.add('off');

        //         setTimeout(() => {
        //             modalBody.innerHTML = document.querySelector('.thanks-module').innerHTML;
        //             modalBody.closest('.modal__inner').classList.add('thanks');
        //             setTimeout(() => {
        //                 modalBody.classList.remove('off');
        //             }, 200);
        //         }, 300);
        //     }
        // });

        let modalBody = form.closest('.modal').querySelector('.modal__content');
        modalBody.classList.add('off');

        setTimeout(() => {
            modalBody.innerHTML = document.querySelector('.thanks-module').innerHTML;
            setTimeout(() => {
                modalBody.closest('.modal__inner').classList.add('thanks');
                modalBody.classList.remove('off');
            }, 300);
        }, 300);

    }

});