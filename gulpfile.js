let gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    del = require('del'),
    autoprefixer = require('gulp-autoprefixer');

gulp.task('html', function () {
    return gulp.src('app/**/*.html')
        .pipe(browserSync.reload({ stream: true }))
})

gulp.task('js', function () {
    return gulp.src('app/js/**/*.js')
        .pipe(browserSync.reload({ stream: true }))
});

gulp.task('scss', function () {
    return gulp.src('app/scss/**/*.scss')
        .pipe(browserSync.reload({ stream: true }))
        .pipe(sass({ outputStyle: "compressed" }))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 8 versions']
        }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('app/css/'))
});

gulp.task('css', function () {
    return gulp.src([
        'node_modules/normalize.css/normalize.css',
        'node_modules/slick-carousel/slick/slick.css',
        'node_modules/animate.css/animate.css',
        'node_modules/magnific-popup/dist/magnific-popup.css'
    ])
        .pipe(concat('_libs.scss'))
        .pipe(gulp.dest('app/scss/'))
});


gulp.task('jsLibs', function () {
    return gulp.src([
        'node_modules/slick-carousel/slick/slick.js',
        'node_modules/magnific-popup/dist/jquery.magnific-popup.js',
        'node_modules/jquery-mask-plugin/dist/jquery.mask.js',
        'node_modules/wow.js/dist/wow.js'
    ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify()) 
        .pipe(gulp.dest('app/js/'))
});

gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "app/"
        }
    });
});

gulp.task('clean', async function () {
    await del.sync('dist');
});

gulp.task('export', async function () {
    let html = gulp.src('app/**/*.html')
        .pipe(gulp.dest('dist/'));

    let css = gulp.src('app/css/**/*.css')
        .pipe(gulp.dest('dist/css/'));

    let js = gulp.src('app/js/**/*.js')
        .pipe(gulp.dest('dist/js/'));

    let img = gulp.src('app/img/**/*.*')
        .pipe(gulp.dest('dist/img/'));

    let fonts = gulp.src('app/fonts/**/*.*')
        .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('build', gulp.series('clean', 'export'));

gulp.task('watch', function () {
    gulp.watch('app/*.html', gulp.series('html', 'build'));
    gulp.watch('app/scss/**/*.scss', gulp.series('scss', 'build'));
    gulp.watch('app/js/*.js', gulp.series('js', 'build'));
});

gulp.task('default', gulp.series(gulp.series(gulp.parallel('css', 'scss', 'jsLibs'), 'build'), gulp.parallel('browser-sync', 'watch')));